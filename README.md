# Docker build

This job configuration allows you to create a docker-image of the application.

### How to include
```yml
stages:
  - build

include:
  - project: 'aanaz/devops-includes/gitlab-ci-extends/includes/docker-build'
    ref: main
    file: docker-build.yml

docker-build:
  extends: .docker-build
  variables:
    DOCKER_BUILD_IMAGE_NAME:
    DOCKER_BUILD_DOCKERFILE_PATH:
    DOCKER_BUILD_CONTEXT:
    DOCKER_BUILD_ARGS:
```

### Variables

| VARIABLE | BY DEFAULT | DESCRIPTION |
| --- | :---: | --- |
`DOCKER_BUILD_IMAGE_NAME` | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}` | Name of image
`DOCKER_BUILD_DOCKERFILE_PATH` | `Dockerfile` | Path to the file with build-instructions
`DOCKER_BUILD_CONTEXT` | `.` | Docker build context
`DOCKER_BUILD_ARGS` | `''` | Build args (usage: `moo=cow woof=dog`)
